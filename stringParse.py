import xml.etree.ElementTree as ET

olz = ET.parse("./stringOld.xml")
nez = ET.parse("./stringNew.xml")

olroot = olz.getroot()
old = {}
neroot = nez.getroot()
new = {}

# print olroot[0].tag, olroot[0].get('name'), olroot[0].text
for oChild in olroot.findall('string'):
    old[oChild.get("name")] = oChild.text

for nchild in neroot.findall('string'):
    if (old.has_key(nchild.get('name')) and old[nchild.get('name')] != nchild.text):
        nchild.text = old[nchild.get('name')]
        print "updated", nchild.get('name')
nez.write("updated.xml")