# import untangle
#
# olz = untangle.parse("./stringOld.xml")
# nez = untangle.parse("./stringNew.xml")
#
# for oChild in olz.resources.string:
#     print oChild['name'], oChild['value']

import xml.etree.ElementTree as ET

oltree = ET.parse('./stringOld.xml')
olroot = oltree.getroot()
old = {}

nutree = ET.parse('./stringNew.xml')
nuroot = nutree.getroot()

for xchild in olroot.findall('string-array'):
    print xchild.get('name')
    i=0
    for xx in xchild.findall('item'):
        old[xchild.get('name')+str(i)] = xx.text
        i = i + 1

for ychild in nuroot.findall('string-array'):
    i=0
    for yy in ychild.findall('item'):
        if (old.has_key(ychild.get('name')+str(i)) and old[ychild.get('name')+str(i)] != yy.text):
            yy.text = old[ychild.get('name')+str(i)]
        i = i + 1
nutree.write("updates.xml")